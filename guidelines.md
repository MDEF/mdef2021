---
title: Documentation Guidelines
description: Documentation for the Master in Design for Emergent Futures
note: The documentation will be reviewed by Master
  directors and coordinators, as well as by the tutors
  responsible for the specific seminar or workshop.
# the image is the CSS classname of the image
image: bg-student
layout: page
---

![document please](https://media.giphy.com/media/xT4uQwLt2AyurOGWFW/giphy.gif)


### How should you document your work?

1. Include videos, images, animations, or any other form of representation to support your written documentation. Every documentation piece should contain, at least, one featured image/animation/video. Apart from the featured visual resources, others could be added to the other parts of the documentation described below.  
2. You are required to write a post of at least 500 words responding the following questions:  
  - What did you do that is new. What did you learn?   
  - How can your learning process be described? Please use bullet points. 
  - How is this relevant to your work and personal development?  
3. You could add additional information and resources to better explain and represent your process and experience. 
4. Each seminar/workshop might include additional documentation requirements. 
5. Once your documentation is ready, commit your changes in GitLab.  
5. After changes are committed, go to the preview page and print a PDF to be saved in your computer.    
6. At the end of each term you will be asked to upload the documentation PDFs in the *Name of the Course // SUBMISSIONS* shared folder in Google Drive. Go to each course folder and create a folder with your name inside and save the PDF for the course in the folder with your name.    
7. You are Done!
